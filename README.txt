In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

Alex Drake (drakeag) Project 2
3/1/2019

Information

It looks like I didn't add three extra pieces of information on the track
detail page, but I did.

The three extra pieces of information I added were:
* Artist name
* Artist website url
* Playcount

The artist website url is embedded as the artist name so a user can click it
and open their Last.fm website. 

I had issues figuring out the JSON parsing, so I chose to implement XML
parsing instead.

Creative Portion

For my creative portion I decided to implement long hold add/remove
from playlist functionality. On the Top Tracks and Results fragments
the user can long hold a track to add it to the playlist if it's not
already added, or remove it from the playlist if it is. When a track
is in the playlist it's track name is bolded. On the Playlist fragment
when a user long holds a track it is automatically removed from the
playlist view and on the other views the name will be unbolded,
indicating it is no longer in the playlist.

Implementing this feature required figuring out how to use a shared
view model instead of separate instances in order to get the fragments
to communicate changes in the data lists. Because all of the data was
shared, when one fragment added or removed a track it automatically
showed up in the other fragment without any special code.

More information: https://developer.android.com/topic/libraries/architecture/viewmodel.html#sharing

(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(8 / 15 points) Creative portion: Be creative!

Everything worked exactly like you described which is great. I do think your creative portion was a little lacking however, as it didn't include anything new, only a
different way to access existing functionality.

Total: 93 / 100