package com.example.playlistapp.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.AsyncTask
import android.util.Log
import com.example.playlistapp.db.DbSettings
import com.example.playlistapp.db.TrackDatabaseHelper
import com.example.playlistapp.enum.QueryMethod
import com.example.playlistapp.enum.TrackImageSize
import com.example.playlistapp.model.Artist
import com.example.playlistapp.model.Track
import com.example.playlistapp.util.QueryUtils

class TrackViewModel(application: Application): AndroidViewModel(application) {
    private val LogTag = this::class.java.simpleName
    private var _trackDBHelper: TrackDatabaseHelper = TrackDatabaseHelper(application)
    private var _tracksList: MutableLiveData<ArrayList<Track>> = MutableLiveData()
    private var _playlist: MutableLiveData<ArrayList<Track>> = MutableLiveData()

    fun getTopTracks(): MutableLiveData<ArrayList<Track>> {
        Log.d(LogTag, "querying for top tracks")
        loadTracks("", QueryMethod.TOP_TRACK)
        return _tracksList
    }

    fun getTracksByArtist(query: String): MutableLiveData<ArrayList<Track>> {
        Log.d(LogTag, "querying for $query")
        loadTracks(query, QueryMethod.ARTIST_SEARCH)
        return _tracksList
    }

    private fun loadTracks(query: String, queryMethod: QueryMethod) {
        TrackAsyncTask().execute(query, queryMethod.toString())
    }

    fun getPlaylist(): MutableLiveData<ArrayList<Track>> {
        //val returnList = this.loadPlaylist()
        //this._playlist.value = returnList
        DatabaseAsyncTask().execute()
        Log.d(LogTag, "loaded ${this._playlist.value?.size} tracks for playlist")
        return this._playlist
    }

    private fun loadPlaylist(): ArrayList<Track> {
        val playlist: ArrayList<Track> = ArrayList()
        val database = this._trackDBHelper.readableDatabase

        val cursor = database.query(
            DbSettings.DBTrackEntry.TABLE,   // The table to query
            null,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,                   // don't group the rows
            null,                   // don't filter by row groups
            null               // The sort order
        )

        while (cursor.moveToNext()) {
            val cursorId = cursor.getColumnIndex(DbSettings.DBTrackEntry.ID)
            val cursorName = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_NAME)
            val cursorDuration = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_DURATION)
            val cursorPlaycount = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_PLAYCOUNT)
            val cursorListeners = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_LISTENERS)
            val cursorMbid = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_MBID)
            val cursorUrl = cursor.getColumnIndex(DbSettings.DBTrackEntry.COL_URL)
            val cursorArtistId = cursor.getColumnIndex(DbSettings.DBTrackEntry.ARTIST_ID)

            // Get artist information
            val artistCursor = database.query(
                DbSettings.DBArtistEntry.TABLE,
                arrayOf(
                    DbSettings.DBArtistEntry.COL_NAME,
                    DbSettings.DBArtistEntry.COL_MBID,
                    DbSettings.DBArtistEntry.COL_URL
                ),
                "${DbSettings.DBArtistEntry.ID}=?", arrayOf(cursor.getLong(cursorArtistId).toString()), null, null, null
            )
            // There should only be one artist
            var artistName = ""
            var artistMbid = ""
            var artistUrl  = ""
            while (artistCursor.moveToNext()) {
                artistName = artistCursor.getString(artistCursor.getColumnIndex(DbSettings.DBArtistEntry.COL_NAME))
                artistMbid = artistCursor.getString(artistCursor.getColumnIndex(DbSettings.DBArtistEntry.COL_MBID))
                artistUrl = artistCursor.getString(artistCursor.getColumnIndex(DbSettings.DBArtistEntry.COL_URL))
            }
            val artist = Artist(artistName, artistMbid, artistUrl)
            artistCursor.close()


            // Get images
            val imgCursor = database.query(
                DbSettings.DBImageAssetEntry.TABLE,
                arrayOf(
                    DbSettings.DBImageAssetEntry.COL_SIZE,
                    DbSettings.DBImageAssetEntry.COL_URL
                ),
                "${DbSettings.DBImageAssetEntry.TRACK_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val images = HashMap<TrackImageSize, String>()
            while (imgCursor.moveToNext()) {
                val key = TrackImageSize.valueOf(imgCursor.getString(imgCursor.getColumnIndex(DbSettings.DBImageAssetEntry.COL_SIZE)))
                val value = imgCursor.getString(imgCursor.getColumnIndex(DbSettings.DBImageAssetEntry.COL_URL))
                images[key] = value
            }
            imgCursor.close()

            val track = Track(
                name = cursor.getString(cursorName),
                duration = cursor.getInt(cursorDuration),
                playcount = cursor.getInt(cursorPlaycount),
                listeners = cursor.getInt(cursorListeners),
                mbid = cursor.getString(cursorMbid),
                urlString = cursor.getString(cursorUrl),
                album = "",
                artist = artist,
                images = images
            )
            track.isInPlaylist = true
            playlist.add(track)
        }

        cursor.close()
        database.close()

        return playlist
    }

    @SuppressLint("StaticFieldLeak")
    inner class TrackAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            Log.d(LogTag, "executing TrackAsycTask")
            return QueryUtils.fetchTrackData(params[0]!!, QueryMethod.valueOf(params[1]!!))
        }

        override fun onPostExecute(result: ArrayList<Track>?) {
            if (result == null) {
                Log.e(LogTag, "No Results Found")
            }
            else {
                //Log.d(LogTag, result.toString())
                // Tag tracks already in the playlist
                val tracks = this@TrackViewModel.loadPlaylist()
                val resultList = ArrayList<Track>()
                for (item in result) {
                    for (track in tracks) {
                        if (track.key == item.key) {
                            item.isInPlaylist = true
                        }
                    }
                    resultList.add(item)
                }
                _tracksList.value = resultList
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class DatabaseAsyncTask: AsyncTask<String, Unit, ArrayList<Track>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Track>? {
            Log.d(LogTag, "executing DatabaseAsyncTask")
            return loadPlaylist()
        }

        override fun onPostExecute(result: ArrayList<Track>?) {
            var resultList = ArrayList<Track>()
            if (result == null) {
                Log.e(LogTag, "No Results Found")
            }
            else {
                resultList = result
            }
            _playlist.value = resultList
        }
    }

    fun addTrack(track: Track) {
        val database: SQLiteDatabase = this._trackDBHelper.writableDatabase

        // Add the artist first to get the artist ID
        val artistValues = ContentValues()
        artistValues.put(DbSettings.DBArtistEntry.COL_NAME, track.artist?.name)
        artistValues.put(DbSettings.DBArtistEntry.COL_MBID, track.artist?.mbid)
        artistValues.put(DbSettings.DBArtistEntry.COL_URL, track.artist?.urlString)
        val artistId = database.insertWithOnConflict(
            DbSettings.DBArtistEntry.TABLE,
            null,
            artistValues,
            SQLiteDatabase.CONFLICT_REPLACE
        )

        val trackValues = ContentValues()
        trackValues.put(DbSettings.DBTrackEntry.COL_KEY, track.key)
        trackValues.put(DbSettings.DBTrackEntry.COL_NAME, track.name)
        trackValues.put(DbSettings.DBTrackEntry.COL_DURATION, track.duration)
        trackValues.put(DbSettings.DBTrackEntry.COL_PLAYCOUNT, track.playcount)
        trackValues.put(DbSettings.DBTrackEntry.COL_LISTENERS, track.listeners)
        trackValues.put(DbSettings.DBTrackEntry.COL_MBID, track.mbid)
        trackValues.put(DbSettings.DBTrackEntry.COL_URL, track.urlString)
        trackValues.put(DbSettings.DBTrackEntry.ARTIST_ID, artistId)
        val trackId = database.insertWithOnConflict(
            DbSettings.DBTrackEntry.TABLE,
            null,
            trackValues,
            SQLiteDatabase.CONFLICT_REPLACE
        )

        for (img in track.images) {
            val imgValues = ContentValues()
            imgValues.put(DbSettings.DBImageAssetEntry.TRACK_ID, trackId)
            imgValues.put(DbSettings.DBImageAssetEntry.COL_SIZE, img.key.toString())
            imgValues.put(DbSettings.DBImageAssetEntry.COL_URL, img.value)
            database.insertWithOnConflict(
                DbSettings.DBImageAssetEntry.TABLE,
                null,
                imgValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }
        /*val tracks = this._tracksList.value
        if (tracks != null) {
            tracks.add(track)
        }
        this._tracksList.value = tracks*/

        track.isInPlaylist = true
        _playlist.value?.add(track)
        _playlist.value = _playlist.value // Triggers an update
        Log.d(LogTag, "added ${track.name} to playlist")
        database.close()
    }

    fun removeTrack(key: String, isFromPlaylist: Boolean = false) {
        val database = _trackDBHelper.writableDatabase

        database.delete(
            DbSettings.DBTrackEntry.TABLE,
            "${DbSettings.DBTrackEntry.COL_KEY}=?",
            arrayOf(key)
        )
        database.close()

        var index = 0
        val playlistTracks = this._playlist.value

        if (playlistTracks != null) {
            //Log.d(LogTag, "Tracks size before removal: ${tracks?.size}")
            val track = playlistTracks.find{ t-> t.key == key}

            if (track != null) {
                track.isInPlaylist = false // Not really necessary
                if (playlistTracks.remove(track)) {
                    //Log.d(LogTag, "Tracks size before removal: ${tracks?.size}")
                    Log.d(LogTag, "removed ${track.name} from playlist")

                    // If we got this far we need to still set the track in the _tracklist list to not in the playlist
                    val trackList = _tracksList.value
                    if (trackList != null) {
                        val trackResult = trackList.find { t -> t.key == key }
                        if (trackResult != null) {
                            trackResult.isInPlaylist = false
                        }
                    }

                    // Trigger the playlist update to show removed track
                    this._playlist.value = playlistTracks
                }
            }
        }
    }
}