package com.example.playlistapp.model

import com.example.playlistapp.enum.TrackImageSize
import java.io.Serializable

class Track(): Serializable {

    val key: String
        get() = "${artist?.key}-$name"

    var name: String = ""
        private set

    var duration: Int = 0
        private set

    var playcount: Int = 0
        private set

    var listeners: Int = 0
        private set

    var mbid: String = ""
        private set

    var urlString: String = ""
        private set

    var artist: Artist? = null
        private set

    var images: HashMap<TrackImageSize, String> = HashMap()
        private set

    var album: String = ""

    var isInPlaylist = false

    constructor(
        name: String,
        duration: Int,
        playcount: Int,
        listeners: Int,
        mbid: String,
        urlString: String,
        artist: Artist,
        images: HashMap<TrackImageSize, String>,
        album: String
    ) : this() {
        this.name = name
        this.duration = duration
        this.playcount = playcount
        this.listeners = listeners
        this.mbid = mbid
        this.urlString = urlString
        this.artist = artist
        this.images = images
        this.album = album
    }

    // Get the largest image available
    fun getImageUrl(): String {
        var imgUrl = ""
        var biggestImgKey = images.keys.max()
        if (biggestImgKey != null) {
            imgUrl = images[biggestImgKey].toString()
        }
        return imgUrl
    }
}