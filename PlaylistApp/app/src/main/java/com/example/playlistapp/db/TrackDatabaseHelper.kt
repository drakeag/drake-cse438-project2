package com.example.playlistapp.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class TrackDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTracksTableQuery = "CREATE TABLE " + DbSettings.DBTrackEntry.TABLE + " ( " +
                DbSettings.DBTrackEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBTrackEntry.COL_KEY + " TEXT NULL, " +
                DbSettings.DBTrackEntry.COL_NAME + " TEXT NULL, " +
                DbSettings.DBTrackEntry.COL_DURATION + " INTEGER NULL, " +
                DbSettings.DBTrackEntry.COL_PLAYCOUNT + " INTEGER NULL, " +
                DbSettings.DBTrackEntry.COL_LISTENERS + " INTEGER NULL, " +
                DbSettings.DBTrackEntry.COL_MBID + " TEXT NULL, " +
                DbSettings.DBTrackEntry.COL_URL + " TEXT NULL, " +
                DbSettings.DBTrackEntry.ARTIST_ID + " INTEGER NULL, " +
                " FOREIGN KEY(" + DbSettings.DBTrackEntry.ARTIST_ID + ") " +
                "REFERENCES " + DbSettings.DBArtistEntry.TABLE + "(" + DbSettings.DBArtistEntry.ID + ") ON DELETE CASCADE);"

        val createImageAssetTableQuery = "CREATE TABLE " + DbSettings.DBImageAssetEntry.TABLE + " ( " +
                DbSettings.DBImageAssetEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBImageAssetEntry.TRACK_ID + " INTEGER NOT NULL, " +
                DbSettings.DBImageAssetEntry.COL_SIZE + " TEXT NOT NULL, " +
                DbSettings.DBImageAssetEntry.COL_URL + " TEXT NULL, " +
                "CONSTRAINT fk_tracks FOREIGN KEY(" + DbSettings.DBImageAssetEntry.TRACK_ID + ") " +
                "REFERENCES " + DbSettings.DBTrackEntry.TABLE + "(" + DbSettings.DBTrackEntry.ID + ") ON DELETE CASCADE);"

        val createArtistTableQuery = "CREATE TABLE " + DbSettings.DBArtistEntry.TABLE + " ( " +
                DbSettings.DBArtistEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBArtistEntry.COL_NAME + " TEXT NULL, " +
                DbSettings.DBArtistEntry.COL_MBID + " TEXT NULL, " +
                DbSettings.DBArtistEntry.COL_URL + " TEXT NULL);"

        db?.execSQL(createTracksTableQuery)
        db?.execSQL(createImageAssetTableQuery)
        db?.execSQL(createArtistTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBTrackEntry.TABLE)
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBImageAssetEntry.TABLE)
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBArtistEntry.TABLE)
        onCreate(db)
    }
}