package com.example.playlistapp.db

import android.provider.BaseColumns

class DbSettings {
    companion object {
        const val DB_NAME = "tracks.db"
        const val DB_VERSION = 1
    }

    class DBTrackEntry: BaseColumns {
        companion object {
            const val TABLE = "tracks"
            const val ID = BaseColumns._ID
            const val COL_KEY = "key"
            const val COL_NAME = "name"
            const val COL_DURATION = "duration"
            const val COL_PLAYCOUNT = "playcount"
            const val COL_LISTENERS = "listeners"
            const val COL_MBID = "mbid"
            const val COL_URL = "url"
            const val ARTIST_ID = "artist_id"
        }
    }

    class DBImageAssetEntry: BaseColumns {
        companion object {
            const val TABLE = "images"
            const val ID = BaseColumns._ID
            const val TRACK_ID = "track_id"
            const val COL_SIZE = "size"
            const val COL_URL = "img_url"
        }
    }

    class DBArtistEntry: BaseColumns {
        companion object {
            const val TABLE = "artists"
            const val ID = BaseColumns._ID
            const val COL_NAME = "name"
            const val COL_MBID = "mbid"
            const val COL_URL = "url"
        }
    }
}