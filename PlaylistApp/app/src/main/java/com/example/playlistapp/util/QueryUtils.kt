package com.example.playlistapp.util

import android.app.DownloadManager
import android.util.Log
import com.example.playlistapp.enum.QueryMethod
import com.example.playlistapp.model.Track
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL


class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val APIKey = "f220feaf3bfc80f4df9c86d83f3e6c19"
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/?api_key=$APIKey"

        fun fetchTrackData(queryString: String, queryMethod: QueryMethod): ArrayList<Track>? {
            var trackList = ArrayList<Track>()
            var url = ""

            when (queryMethod) {
                QueryMethod.TOP_TRACK -> url = "${this.BaseURL}&method=chart.gettoptracks"
                QueryMethod.ARTIST_SEARCH -> url = "${this.BaseURL}&method=artist.gettoptracks&artist=$queryString"
            }

            Log.d(this.LogTag, "making HTTP request to $url")

            try {
                trackList = makeHttpRequest(url)?.use { stream ->
                    // Instantiate the parser
                    when (queryMethod) {
                        QueryMethod.TOP_TRACK ->TopTracksXmlParser.parseTopTracks(stream)
                        QueryMethod.ARTIST_SEARCH -> ArtistTracksXmlParser.parseArtistTracks(stream)
                    }
                } ?: ArrayList()
                Log.d(this.LogTag, "HTTP request successful.")
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return trackList
        }

        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        // Given a string representation of a URL, sets up a connection and gets
        // an input stream.
        @Throws(IOException::class)
        private fun makeHttpRequest(urlString: String): InputStream? {
            val url = URL(urlString)
            return (url.openConnection() as? HttpURLConnection)?.run {
                readTimeout = 10000
                connectTimeout = 15000
                requestMethod = "GET"
                doInput = true
                // Starts the query
                connect()
                inputStream
            }
        }

    }
}
