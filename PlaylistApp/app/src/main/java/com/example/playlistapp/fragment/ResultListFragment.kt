package com.example.playlistapp.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.playlistapp.R
import com.example.playlistapp.activity.TrackDetailActivity
import com.example.playlistapp.model.Track
import com.example.playlistapp.viewmodel.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.track_list_item.view.*


@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    private val LogTag = this::class.java.simpleName
    private var adapter = ResultAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: TrackViewModel
    private var listInitialized = false

    private var queryString: String = query
    private var trackList: ArrayList<Track> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        result_items_list.layoutManager =  GridLayoutManager(parentContext, 2) //LinearLayoutManager(parentContext)

        // Shared view model (the scope is the parent activity)
        viewModel = activity?.run {
            Log.d(LogTag, "Activity: ${this}")
            ViewModelProviders.of(this).get(TrackViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val observer = Observer<ArrayList<Track>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0].key == trackList[p1].key
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (trackList[p0] == trackList[p1])
                }
            })
            result.dispatchUpdatesTo(adapter)
            trackList = it ?: ArrayList()
        }

        viewModel.getTracksByArtist(queryString).observe(this, observer)

        this.listInitialized = true
    }

    inner class ResultAdapter: RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.track_list_item, p0, false)
            return ResultViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
            val track = trackList[p1]
            val trackImages = track.images
            if (trackImages.size == 0) {
                // Do nothing for now
            }
            else {
                val imgUrl = track.getImageUrl()
                if (imgUrl.isNullOrBlank() == false) {
                    Picasso.with(this@ResultListFragment.parentContext).load(imgUrl).into(p0.trackImg)
                }
            }
            p0.trackName.text = track.name
            setTextColor(track, p0)

            // This will open a new activity when a track is clicked
            p0.row.setOnClickListener {
                val intent = Intent(this@ResultListFragment.parentContext, TrackDetailActivity::class.java)
                intent.putExtra("TRACK", track)
                startActivity(intent)
            }

            // This will open add/remove a track if a track is long held
            p0.row.setOnLongClickListener {
                if (track.isInPlaylist) {
                    try {
                        viewModel.removeTrack(track.key)
                        Toast.makeText(parentContext, "Successfully removed track", Toast.LENGTH_SHORT).show()
                    }
                    catch (e: Exception) {
                        Toast.makeText(parentContext, "Unable to remove track", Toast.LENGTH_SHORT).show()
                        Log.e(LogTag, "error removing track from playlist: ${e.message}")
                    }
                }
                else
                {
                    try {
                        viewModel.addTrack(track)
                        Toast.makeText(parentContext, "Successfully added track", Toast.LENGTH_SHORT).show()
                    }
                    catch (e: Exception) {
                        Toast.makeText(parentContext, "Unable to add track", Toast.LENGTH_SHORT).show()
                        Log.e(LogTag, "error adding track to playlist: ${e.message}")
                    }
                }
                setTextColor(track, p0)
                return@setOnLongClickListener true
            }
        }

        private fun setTextColor(track: Track, p0: ResultViewHolder) {
            if (track.isInPlaylist) {
                p0.trackName.typeface = Typeface.DEFAULT_BOLD
            }
            else {
                p0.trackName.typeface = Typeface.DEFAULT
            }
        }

        override fun getItemCount(): Int {
            return trackList.size
        }

        inner class ResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var trackImg: ImageView = itemView.track_cover_img
            var trackName: TextView = itemView.track_name_tv
        }
    }
}