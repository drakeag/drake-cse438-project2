package com.example.playlistapp.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.ArrayList
import android.arch.lifecycle.Observer
import android.graphics.Typeface
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.widget.Toast
import com.example.playlistapp.R
import com.example.playlistapp.activity.TrackDetailActivity
import com.example.playlistapp.model.Track
import com.example.playlistapp.viewmodel.TrackViewModel
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.playlist_list_item.view.*


@SuppressLint("ValidFragment")
class PlaylistFragment(context: Context): Fragment() {
    private val LogTag = this::class.java.simpleName
    private var adapter = PlaylistAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: TrackViewModel

    private var trackList: ArrayList<Track> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onStart() {
        super.onStart()
        playlist_list.layoutManager = GridLayoutManager(context, 2)

        // Shared view model (the scope is the parent activity)
        viewModel = activity?.run {
            Log.d(LogTag, "Activity: ${this}")
            ViewModelProviders.of(this).get(TrackViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val observer = Observer<ArrayList<Track>> {
            playlist_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    Log.d(LogTag, "calling are same on index $p0 and $p1 (size=${trackList.size}")
                    if (p1 >= trackList.size) {
                        Log.e(LogTag, "index out of bounds!")
                        return false
                    }
                    return trackList[p0].key == trackList[p1].key
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            trackList = it ?: ArrayList()
        }

        viewModel.getPlaylist().observe(this, observer)
    }

    inner class PlaylistAdapter: RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PlaylistViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.playlist_list_item, p0, false)
            return PlaylistViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: PlaylistViewHolder, p1: Int) {
            val track = trackList[p1]
            val trackImages = track.images
            if (trackImages.size == 0) {
                // Do nothing for now
            }
            else {
                val imgUrl = track.getImageUrl()
                if (imgUrl.isNullOrBlank() == false) {
                    Picasso.with(this@PlaylistFragment.parentContext).load(imgUrl).into(p0.trackImg)
                }
            }
            p0.trackName.text = track.name
            setTextColor(track, p0)

            p0.row.setOnClickListener {
                val intent = Intent(this@PlaylistFragment.parentContext, TrackDetailActivity::class.java)
                intent.putExtra("TRACK", track)
                startActivity(intent)
            }

            // This will open add/remove a track if a track is long held
            p0.row.setOnLongClickListener {
                if (track.isInPlaylist) {
                    try {
                        viewModel.removeTrack(track.key, true)
                        Toast.makeText(parentContext, "Successfully removed track", Toast.LENGTH_SHORT).show()
                    }
                    catch (e: Exception) {
                        Toast.makeText(parentContext, "Unable to remove track", Toast.LENGTH_SHORT).show()
                        Log.e(LogTag, "error removing track from playlist: ${e.message}")
                    }
                }
                else
                {
                    try {
                        viewModel.addTrack(track)
                        Toast.makeText(parentContext, "Successfully added track", Toast.LENGTH_SHORT).show()
                    }
                    catch (e: Exception) {
                        Toast.makeText(parentContext, "Unable to add track", Toast.LENGTH_SHORT).show()
                        Log.e(LogTag, "error adding track to playlist: ${e.message}")
                    }
                }
                return@setOnLongClickListener true
            }
        }

        private fun setTextColor(track: Track, p0: PlaylistViewHolder) {
            if (track.isInPlaylist) {
                p0.trackName.typeface = Typeface.DEFAULT_BOLD
            }
            else {
                p0.trackName.typeface = Typeface.DEFAULT
            }
        }

        override fun getItemCount(): Int {
            return trackList.size
        }

        inner class PlaylistViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var row = itemView

            var trackImg: ImageView = itemView.playlist_track_cover_img
            var trackName: TextView = itemView.playlist_track_name_tv
        }
    }
}
