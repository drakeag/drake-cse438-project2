package com.example.playlistapp.util

import android.util.Log
import android.util.Xml
import com.example.playlistapp.enum.TrackImageSize
import com.example.playlistapp.model.Track
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream
import com.example.playlistapp.model.Artist

class TopTracksXmlParser {
    companion object {
        private val LogTag = this::class.java.simpleName
        private val ns: String? = null

        @Throws(XmlPullParserException::class, IOException::class)
        fun parseTopTracks(inputStream: InputStream): ArrayList<Track> {
            inputStream.use { inputStream ->
                val parser: XmlPullParser = Xml.newPullParser()
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                parser.setInput(inputStream, null)
                parser.nextTag()
                return readLfm(parser)
            }
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readLfm(parser: XmlPullParser): ArrayList<Track> {
            val tracks = ArrayList<Track>()

            parser.require(XmlPullParser.START_TAG, ns, "lfm")
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                // Starts by looking for the entry tag
                if (parser.name == "tracks") {
                    tracks.addAll(readTracks(parser))
                } else {
                    skip(parser)
                }
            }
            return tracks
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readTracks(parser: XmlPullParser): ArrayList<Track> {
            val tracksList = ArrayList<Track>()

            parser.require(XmlPullParser.START_TAG, ns, "tracks")
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                // Starts by looking for the entry tag
                if (parser.name == "track") {
                    tracksList.add(readTrack(parser))
                } else {
                    skip(parser)
                }
            }

            Log.d(LogTag, "parsed ${tracksList.size} tracks")
            return tracksList
        }

        // Parses the contents of an entry. If it encounters a title, summary, or link tag,
        // hands them off  to their respective "read" methods for processing. Otherwise,
        // skips the tag.
        @Throws(XmlPullParserException::class, IOException::class)
        private fun readTrack(parser: XmlPullParser): Track {
            parser.require(XmlPullParser.START_TAG, ns, "track")
            var name = ""
            var duration = -1
            var playcount = -1
            var listeners = -1
            var mbid = ""
            var url = ""
            var artist = Artist("NULL", "NULL", "NULL")
            var images = HashMap<TrackImageSize, String>()

            //var link: String? = null
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                when (parser.name) {
                    "name" -> name = readTagText(parser, "name")
                    "duration" -> duration = readTagText(parser, "duration").toInt()
                    "playcount" -> playcount = readTagText(parser, "playcount").toInt()
                    "listeners" -> listeners = readTagText(parser, "listeners").toInt()
                    "mbid" -> mbid = readTagText(parser, "mbid")
                    "url" -> url = readTagText(parser, "url")
                    "artist" -> artist = readArtist(parser)
                    "image" ->
                    {
                        var kvm = readImage(parser)
                        images[kvm.first] = kvm.second
                    }
                    else -> skip(parser)
                }
            }

            return Track(
                name = name,
                duration = duration,
                playcount = playcount,
                listeners =  listeners,
                mbid = mbid,
                urlString =  url,
                artist = artist,
                images = images,
                album = "album"
            )
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readArtist(parser: XmlPullParser): Artist {
            parser.require(XmlPullParser.START_TAG, ns, "artist")
            var name = ""
            var mbid = ""
            var url = ""

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "name" -> name = readTagText(parser, "name")
                    "mbid" -> mbid = readTagText(parser, "mbid")
                    "url" -> url = readTagText(parser, "url")
                    else -> skip(parser)
                }
            }

            return Artist(
                name = name,
                mbid = mbid,
                urlString = url
            )
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readImage(parser: XmlPullParser): Pair<TrackImageSize, String> {
            parser.require(XmlPullParser.START_TAG, ns, "image")

            var imgSize = TrackImageSize.SMALL
            var imgUrl = ""

            var imgSizeStr = parser.getAttributeValue(null, "size")
            when (imgSizeStr) {
                "small" -> imgSize = TrackImageSize.SMALL
                "medium" -> imgSize = TrackImageSize.MEDIUM
                "large" -> imgSize = TrackImageSize.LARGE
                "extralarge" -> imgSize = TrackImageSize.EXTRA_LARGE
            }

            if (parser.next() == XmlPullParser.TEXT) {
                imgUrl = parser.text
                parser.nextTag()
            }
            parser.require(XmlPullParser.END_TAG, ns, "image")

            return Pair(imgSize, imgUrl)
        }

        // Processes title tags in the feed.
        @Throws(IOException::class, XmlPullParserException::class)
        private fun readTagText(parser: XmlPullParser, tag: String): String {
            parser.require(XmlPullParser.START_TAG, ns, tag)
            val text = readText(parser)
            parser.require(XmlPullParser.END_TAG, ns, tag)
            return text
        }

        // For the tags title and summary, extracts their text values.
        @Throws(IOException::class, XmlPullParserException::class)
        private fun readText(parser: XmlPullParser): String {
            var result = ""
            if (parser.next() == XmlPullParser.TEXT) {
                result = parser.text
                parser.nextTag()
            }
            return result
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun skip(parser: XmlPullParser) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                throw IllegalStateException()
            }
            var depth = 1
            while (depth != 0) {
                when (parser.next()) {
                    XmlPullParser.END_TAG -> depth--
                    XmlPullParser.START_TAG -> depth++
                }
            }
        }
    }
}