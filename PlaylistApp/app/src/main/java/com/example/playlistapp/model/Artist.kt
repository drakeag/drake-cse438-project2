package com.example.playlistapp.model

import com.example.playlistapp.enum.TrackImageSize
import java.io.Serializable

class Artist(): Serializable {

    val key: String
        get() = "$mbid-$name"

    var name: String = ""
        private set

    var mbid: String = ""
        private set

    var urlString: String = ""
        private set

    constructor(
        name: String,
        mbid: String,
        urlString: String
    ) : this () {
        this.name = name
        this.mbid = mbid
        this.urlString = urlString
    }

}