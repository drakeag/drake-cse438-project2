package com.example.playlistapp.enum

enum class UserInterfaceState {
    NETWORK_ERROR,
    NO_DATA,
    RESET,
    HOME,
    RESULTS
}

enum class TrackImageSize {
    SMALL,
    MEDIUM,
    LARGE,
    EXTRA_LARGE
}

enum class QueryMethod(val value: Int) {
    TOP_TRACK(1),
    ARTIST_SEARCH(2)
}