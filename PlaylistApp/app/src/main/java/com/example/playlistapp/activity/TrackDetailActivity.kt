package com.example.playlistapp.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.playlistapp.R
import com.example.playlistapp.model.Track
import com.example.playlistapp.viewmodel.TrackViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_track_detail.*
import java.lang.Exception

class TrackDetailActivity : AppCompatActivity() {
    private val LogTag = this::class.java.simpleName
    private lateinit var track: Track
    private lateinit var viewModel: TrackViewModel

    //private lateinit var adapter: FeatureAdapter
    //private var features = ArrayList<Feature>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_detail)

        track = intent.extras!!.getSerializable("TRACK") as Track

        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        this.loadUI(track)
    }

    private fun loadUI(track: Track) {
        val imgUrl = track.getImageUrl()
        if (imgUrl.isNullOrBlank() == false) {
            Picasso.with(this).load(track.getImageUrl()).into(track_detail_img)
        }
        track_detail_title_tv.text = track.name
        track_detail_playcount_tv.text = "Playcount: ${track.playcount}"

        // Make clickable artist website link
        val url = "<a href=\"${track.artist?.urlString}\">${track.artist?.name}</a>"
        track_detail_artist_tv.text = Html.fromHtml(url, 0)
        track_detail_artist_tv.movementMethod = LinkMovementMethod.getInstance()

        if (track.isInPlaylist) {
            add_to_playlist_btn.visibility = View.GONE
            remove_from_playlist_btn.visibility = View.VISIBLE
        }
        else {
            add_to_playlist_btn.visibility = View.VISIBLE
            remove_from_playlist_btn.visibility = View.GONE
        }

        add_to_playlist_btn.setOnClickListener {
            try {
                //val db = TrackDatabaseHelper(this)
                this.viewModel.addTrack(track)
                add_to_playlist_btn.visibility = View.GONE
                remove_from_playlist_btn.visibility = View.VISIBLE
                Toast.makeText(this, "Successfully added track", Toast.LENGTH_SHORT).show()
            }
            catch (e: Exception) {
                Toast.makeText(this, "Unable to add track", Toast.LENGTH_SHORT).show()
                Log.e(LogTag, "error adding track to playlist: ${e.message}")
            }
        }

        remove_from_playlist_btn.setOnClickListener {
            try {
                //val db = TrackDatabaseHelper(this)
                this.viewModel.removeTrack(track.key)
                add_to_playlist_btn.visibility = View.VISIBLE
                remove_from_playlist_btn.visibility = View.GONE
                Toast.makeText(this, "Successfully removed track", Toast.LENGTH_SHORT).show()
            }
            catch (e: Exception) {
                Toast.makeText(this, "Unable to remove track", Toast.LENGTH_SHORT).show()
                Log.e(LogTag, "error removing track from playlist: ${e.message}")
            }
        }
    }
}
