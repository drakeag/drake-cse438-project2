package com.example.playlistapp.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import com.example.playlistapp.R
import com.example.playlistapp.enum.UserInterfaceState
import com.example.playlistapp.fragment.HomeFragment
import com.example.playlistapp.fragment.NoConnectionFragment
import com.example.playlistapp.fragment.PlaylistFragment
import com.example.playlistapp.viewmodel.TrackViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val LogTag = this::class.java.simpleName
    var currentView = UserInterfaceState.HOME
    private var isNetworkConnected = false
    //private lateinit var viewModel: TrackViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        //viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        if (networkInfo == null) {
            Log.e("NETWORK", "not connected")
        }
        else {
            Log.d("NETWORK", "connected")
            this.isNetworkConnected = true
        }

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager, this, this.isNetworkConnected)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
    }

    class MyPagerAdapter(fm: FragmentManager, context: Context, isNetworkConnected: Boolean)
        : FragmentPagerAdapter(fm) {
        private var parentContext = context
        private var parentIsNetworkConnected = isNetworkConnected
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    if (parentIsNetworkConnected) {
                        Log.d("MainActivity", "starting HomeFragment")
                        HomeFragment(parentContext)
                    }
                    else {
                        NoConnectionFragment()
                    }
                }
                else ->
                {
                    Log.d("MainActivity", "starting PlaylistFragment")
                    PlaylistFragment(parentContext)
                }
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Home"
                else -> {
                    return "Playlist"
                }
            }
        }
    }
}
